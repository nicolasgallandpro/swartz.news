#!/bin/sh
export PYTHONPATH="$PYTHONPATH:/workspace/sources:/workspace"
pip install feedparser beautifulsoup4 redis
jupyter lab --port=8888 --no-browser --ip=0.0.0.0 --allow-root &

# dagit
cd /opt/dagster/conf
dagster-daemon run &
dagit -h "0.0.0.0" -p "3000" -w "/opt/dagster/conf/workspace.yaml" 



