Create a simple html website from RSS/youtube/substack/... and update mastodon profiles with the posts of the streams.
You can filter and merge differents streams.

....

# accepted input streams
- rss
- youtube
- google news
- substack profiles
- mastodon/firefish profiles
- sitemaps (soon)

# configuration
conf.yaml ...
secrets.json (healthcheck url, mastodon api credentials, ...)
rsc files 
