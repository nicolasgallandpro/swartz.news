def dict_to_markdown(dictionary, level=0):
    markdown_content = ""

    for key, value in dictionary.items():
        # Indentation en fonction du niveau de profondeur
        indentation = "  " * level

        if isinstance(value, dict):
            # Si la valeur est un dictionnaire, appeler la fonction récursivement
            markdown_content += f"{indentation}- {key}:\n"
            markdown_content += dict_to_markdown(value, level + 1)
        elif isinstance(value, list):
            # Si la valeur est une liste, appeler la fonction récursivement pour chaque élément
            markdown_content += f"{indentation}- {key}:\n"
            for item in value:
                if isinstance(item, (dict, list)):
                    markdown_content += dict_to_markdown(item, level + 2)
                else:
                    markdown_content += f"  {indentation}  - {item}\n"
        else:
            # Si la valeur est un type simple, l'ajouter au format Markdown
            markdown_content += f"{indentation}- {key}: {value}\n"

    return markdown_content



def create_page(rsc_filled, basedir, html_file, title, description, json_file, rsc_file):
    description_plus_source = description + f'</br><a href="{rsc_file}" style="border-bottom: 1px solid #7dd3fc">Sources</a>'
    page_template = open('/workspace/sources/template.html').read()
    content = page_template.replace('___TITLE___',title).replace('___DESCRIPTION___',description_plus_source)\
                .replace('___JSON___',json_file)
    #print(out)
    
    #html
    f = open(basedir+html_file,'w')
    f.write(content)
    f.close()
    
    #json
    f = open(basedir+json_file, "w")
    f.write(rsc_filled.to_json())
    f.close()