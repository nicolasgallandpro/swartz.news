from dagster import asset, MetadataValue, Definitions, define_asset_job, ScheduleDefinition
from pandas import DataFrame, read_html, get_dummies
from sklearn.linear_model import LinearRegression as Regression
from commons import *
from rsc import *
from entries import *
import os, yaml

@asset
def streams_conf(context) :
    """Retrieve server conf"""
    with open('/workspace/conf.yaml', 'r') as yaml_file:
        config = yaml.load(yaml_file, yaml.SafeLoader)
    context.add_output_metadata({'configuration':MetadataValue.md(dict_to_markdown(config))}    )
    return config

@asset
def secrets(context) :
    return json.loads(open('/workspace/secrets.json').read())

@asset
def rsc_files(context, streams_conf) :
    """Retrieve rsc files"""
    out = {}
    errors = []
    oks = []
    for key, stream in streams_conf.items():
        context.log.info(f'{key}')
        try:
            if 'rsc' in stream and stream['rsc']:
                additionnal_params = {}
                out[key] = RSC(stream['rsc'], additionnal_params=additionnal_params)
                oks.append(key)
            else :
                context.log.error(f"No rsc entry in stream {key}")
                errors.append(key)
        except:
            errors.append(str(key))
            context.log.error(f"Error while loading rsc url : {stream['rsc']}")
            
    context.add_output_metadata({'stream errors':' - '.join(errors)}    )
    #'configuration':MetadataValue.md(dict_to_markdown(out))
    return out


@asset
def rsc_filled(context, rsc_files):
    """Fill RSC objects with categories, entries (posts), ... """
    out = {}
    for key, rsc in rsc_files.items():
        rsc.log = context.log
        rsc.fill_rsc() 
        rsc.log = None
        out[key] = rsc
    return out

@asset
def html_and_json_files(rsc_filled):
    """For each rsc file, create a json file (contains all the informations and entries) and the html file."""
    for key, rsc in rsc_filled.items():
        create_page(rsc_filled=rsc, basedir='/workspace/STATIC/',json_file=f"{key}.json", html_file=f'{key}.html', title=f'{key}', rsc_file=rsc.rsc_file, description=rsc.description)
    return "OK"

@asset
def mastodon_update(rsc_filled, secrets):
    """Send message to mastodon servers"""
    return ""

@asset
def healthcheck(context, mastodon_update, html_and_json_files, secrets):
    """Healthcheck request"""
    try:
        import requests
        requests.get(secrets['healthcheck']) 
    except KeyError:
        context.log.error('No healthcheck url defined in varenvs')
    except:
        context.log.error(f"Error while requesting {secrets['healthcheck']}")
    return ""



main_assets = [streams_conf, rsc_files, rsc_filled, html_and_json_files, secrets]
all_assets = main_assets + [mastodon_update, healthcheck]
job_complete = define_asset_job(name="complete_job", selection=all_assets)
schedule = ScheduleDefinition(job=job_complete, cron_schedule="0 * * * *")
job_partial = define_asset_job(name="partial_job", selection=main_assets)

defs = Definitions(
    assets=all_assets,
    jobs = [job_complete, job_partial],
    schedules=[schedule]
)