version: 0.1
name: media
description: media news
max_age_minutes: 50400  #50400 min = 5 weeks
categories:
  - 'name': media
    feeds :
      - 'Simon Owens substack' : 'https://simonowens.substack.com/feed'
      - 'Simon Owens youtube' : 'https://www.youtube.com/@nomissnewo'
      - 'Speciall media - Carolyn Morgan' : 'https://speciall.media/articles/feed/'
      - 'The Audiencers Fr' : 'https://theaudiencers.com/fr/feed/'
      - 'The Nieman lab' : 'https://www.niemanlab.org/feed/'
      - 'Inbox collective' : 'https://inboxcollective.com/read-our-latest-stories/feed/'
      - 'The Rebooting' : 'https://www.therebooting.com/feed'
      - 'NYTimes open' : 'https://open.nytimes.com/feed'
      - 'Press Gazette' : 'https://pressgazette.co.uk/comments/feed/'
