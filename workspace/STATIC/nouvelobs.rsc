version: 0.1
name: nico
description: 'sciences, tech, innov, economie, etc'
max_age_minutes: 1440000  #1440 min = 24h
categories:
  - 'name': 'Sciences, tech, innov, economie'
    feeds :
      - 'nouvel obs' : 'https://www.nouvelobs.com/rss.xml'